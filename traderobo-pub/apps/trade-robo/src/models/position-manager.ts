import { PositionType } from '../strategies/models/strategy';
import { Observable } from 'rxjs';

export interface Position {
    id: string;
    instrument: string;
    type: PositionType;
    size: number;
    open: {
        timestamp: number;
        price: number;
    };
    close: {
        timestamp: number;
        price: number;
    };
    opened: boolean;
}

export interface PositionStrategy {
    couldOpen(instrument: string): boolean;

    getPositionSize(instrument): number;

    notifyOpen(position: Position): void;

    notifyClose(id: string): void;
}

export interface PositionManager {
    open(instrument, type: PositionType): void;
    onOpen$(): Observable<Position>;
    onClose$(): Observable<Position>;
}