import { Injectable, Logger } from '@nestjs/common';
import { forkJoin, from, of } from 'rxjs';
import { catchError, finalize, mergeMap } from 'rxjs/operators';

import { MACDZeroCrossStrategy } from '../../strategies/macd-zero-cross-strategy';
import { PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { InstrumentData, InstrumentSubscriptionService, Trading212QueryService } from '@robo-trader/broker-api';
import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';

@Injectable()
export class TradingTaskService {
	private instrument = 'EURUSD';
	private periodType = PeriodType.Minute;
	private stopLoss = 0.0009;
	private emaAngleThreshold = 0.0004;
	private collectedData: {[key in PeriodType]?: CandleData[]};
	private strategy = new MACDZeroCrossStrategy({
		period: this.periodType
	});
	private logger = new Logger('TradingTaskService');

	constructor(
		private queryService: Trading212QueryService,
		private subscriptionService: InstrumentSubscriptionService
	) {
		// this.collectedData = fromPairs(this.strategy.requirements.map(({periodType}) => [periodType, []]));
		// this
		// 	.initTrading()
		// 	.then(() => this.logger.log('Initialization complete'));
	}

	private async initTrading() {
		if (!this.isMarketOpen()) {
			this.scheduleMarketOpenedEvent();
			return;
		}

		this.scheduleMarketWillCloseEvent();

		await this.updateHistoryData();

		this.checkForPosition();

		this.subscriptionService
			.getLatestData$(this.instrument, [this.periodType])
			.subscribe(this.onLatestCandles.bind(this))
	}

	private updateHistoryData() {
		return new Promise<void>(resolve => {
			from([this.periodType])
				.pipe(
					mergeMap(period => forkJoin({
						periodType: of(period),
						candles: this.queryService.getInstrumentData(this.instrument, period, Trading212QueryService.MAX_DATA_SIZE)
					})),
					catchError(error => {
						this.logger.error(error);
						return [];
					}),
					finalize(() => {
						this.logger.log(`[${this.instrument}]\tHistory fetch: Complete`);

						resolve();
					})
				)
				.subscribe(this.setStrategyData.bind(this))
		})
	}

	private setStrategyData({periodType, candles}: {periodType: PeriodType, candles: CandleData[]}) {
		candles.forEach((candle: CandleData) => this.strategy.append(candle.ask, candle.time.unix()));

		this.updateCollectedData(periodType, candles);
		this.logger.log(`[${this.instrument}]\t[${periodType}] Last record time: ${candles.slice(-1).pop().time}`)
	}

	private checkForPosition() {
		// const positionType = this.strategy.couldOpenPosition();
		//
		// if (positionType) {
		// 	// this.positionService.open()
		//
		// 	{
		// 		const candle = this.collectedData.FIFTEEN_MINUTES.slice(-1).pop();
		// 		this.logger.debug(`[${this.instrument}]Opened ${positionType} position at:
		// 			${candle.time}
		// 			${candle.ask.close}
		// 		`
		// 		);
		//
		// 		// this.logger.debug(this.strategy.debug())
		// 	}
		// }
	}

	private scheduleMarketOpenedEvent() {
		return;
	}
	private scheduleMarketWillCloseEvent() {
		return;
	}

	private onMarketOpened() {
		this.initTrading();
	}

	// TODO: Query real data from broker
	private isMarketOpen(): boolean {
		return true;
	}

	private onLatestCandles(dataCollection: InstrumentData[]) {
		this.logger.debug(dataCollection)

		dataCollection
			.forEach(({candle, periodType}) =>
				this.strategy.append(candle.ask, candle.time.unix())
			);
		dataCollection.forEach(collection => this.updateCollectedData(collection.periodType, [collection.candle]));

		this.checkForPosition();
	}

	private updateCollectedData(periodType: PeriodType, candles: CandleData[]) {
		this.collectedData[periodType] = this.collectedData[periodType].concat(candles);
	}
}

