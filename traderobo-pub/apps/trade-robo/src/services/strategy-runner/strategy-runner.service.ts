import { Observable } from 'rxjs';
import { filter, map, tap, finalize } from 'rxjs/operators';

import { CandleData, PriceData } from '@robo-trader/broker-api/models/candle-data.model';
import { PERIOD_TYPE_TIME_VALUE_MAP, PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { PositionType, Strategy } from '../../strategies/models/strategy';
import { SamplesAggregator } from '../../utils/samples-aggregator/samples-aggregator';

export enum PriceSelector {
    ASK,
    BID,
    MEAN
}
const candlePriceSelector = (selector = PriceSelector.MEAN) => (candle: CandleData): PriceData => {
    switch (selector) {
        case PriceSelector.ASK:
            return candle.ask;
        case PriceSelector.BID:
            return candle.bid;
        default:
            return {
                open: ((candle.ask.open + candle.bid.open) / 2),
                close: ((candle.ask.close + candle.bid.close) / 2),
                high: ((candle.ask.high + candle.bid.high) / 2),
                low: ((candle.ask.low + candle.bid.low) / 2),
            };
    }
};

type StrategyRunnerOptions = {
    periodType: PeriodType;
    selector?: PriceSelector;
}

type OnPositionEntryHandler = (position: PositionType) => void;

export class StrategyRunnerService {
    private candles$: Observable<CandleData> = null;
    private readonly priceSelector: (candle: CandleData) => PriceData;

    constructor(private strategy: Strategy<PriceData>, private options: StrategyRunnerOptions) {
        this.priceSelector = candlePriceSelector(this.options?.selector);
    }

    attachCandlesStream(candles$: Observable<CandleData>, periodType: PeriodType) {
        const aggregator = new SamplesAggregator(periodType, this.options.periodType);
        this.candles$ = candles$
            .pipe(aggregator.pipe());
    }

    onEntry(onEntry: OnPositionEntryHandler, onFinish: () => void) {
        this.candles$
            .pipe(
                filter(this.isPeriodEnd),
                map((candle) => {
                    this.strategy.append(
                        this.priceSelector(candle),
                        candle.closedTime.unix() // TODO: Extract logic of time decision
                    );

                    return this.strategy.isPositionEntry();
                }),
                // map(candle => {
                //     const method = this.isPeriodStart(candle) ? this.strategy.append : this.strategy.update;
                //
                //     method.call(
                //         this.priceSelector(candle),
                //         candle.closedTime.unix() // TODO: Extract logic of time decision
                //     );
                //
                //     return this.strategy.isPositionEntry();
                // }),
                tap(), // TODO: place to attach strategy state writable stream, state$.next(this.strategy.snapshot())
                filter(isEntry => isEntry !== false),
                finalize(onFinish)
            )
            .subscribe(onEntry)
    }

    private isPeriodStart(candle: CandleData) {
        return candle.time.unix() % PERIOD_TYPE_TIME_VALUE_MAP[this.options.periodType].asSeconds() === 0;
    }

    private isPeriodEnd = (candle: CandleData) => {
        return candle.closedTime.unix() % PERIOD_TYPE_TIME_VALUE_MAP[this.options.periodType].asSeconds() === 0;
    }
}
