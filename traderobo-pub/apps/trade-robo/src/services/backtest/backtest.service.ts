import { Injectable } from '@nestjs/common';
import { PriceSelector, StrategyRunnerService } from '../strategy-runner/strategy-runner.service';
import { MACDZeroCrossStrategy } from '../../strategies/macd-zero-cross-strategy';
import { InstrumentsCollectionService } from '@robo-trader/data-storage-manager';
import { PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { BacktestPositionManager } from './backtest-position-manager';
import { BacktestQueryServiceHelper } from './utils/backtest-query.service';
import { BacktestPLTracker } from './utils/backtest-pl-tracker';
import { toArray } from 'rxjs/operators';
import { Trading212QueryService } from '@robo-trader/broker-api';

const DEFAULT_STRATEGY = MACDZeroCrossStrategy;

@Injectable()
export class BacktestService {
    // TODO: inject interface QueryService, not implementation
    constructor(private collectionService: InstrumentsCollectionService, private trading212Service: Trading212QueryService) {}

    run(instrument: string, takeProfit: number, stopLoss: number) {
        const queryService = new BacktestQueryServiceHelper();

        const positionManager = new BacktestPositionManager(
            queryService,
            new BacktestPLTracker(queryService)
        );
        positionManager.setTakeProfit(takeProfit);
        positionManager.setStopLoss(stopLoss);

        const runner = new StrategyRunnerService(new DEFAULT_STRATEGY({
            period: PeriodType.FiveMinutes,
        }), {
            periodType: PeriodType.FiveMinutes,
            selector: PriceSelector.MEAN
        });

        runner.attachCandlesStream(
            queryService.getCompleted$(instrument), // QueryService.getInstrumentData$(code, periodType)
            PeriodType.Minute
        );

        runner.onEntry(positionType => {
            console.log('ENTRY')
            positionManager.open(instrument, positionType);
        }, () => {
            console.log('FINISHED')
            positionManager.finish()
        });

        queryService.addInstrumentData$(
            instrument,
            this.collectionService.getInstrumentData$(instrument)
        );

        return positionManager.onClose$().pipe(toArray()).toPromise();
    }
}
