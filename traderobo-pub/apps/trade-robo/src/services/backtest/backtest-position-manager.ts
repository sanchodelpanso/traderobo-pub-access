import { Position, PositionManager, PositionStrategy } from '../../models/position-manager';
import { PositionType } from '../../strategies/models/strategy';
import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';
import { Subject } from 'rxjs';
import { InstrumentQueryService } from '@robo-trader/broker-api/models/instrument-query.service';

export interface ProfitLossTracker {
    addTracking(position: Position, takeProfit: number, stopLoss: number);
    onExit(callback: (id: string, price: number, timestamp: number) => void);
}

class DefaultPositionStrategy implements PositionStrategy {
    couldOpen(instrument: string): boolean {
        return true;
    }

    getPositionSize(instrument): number {
        return 1000;
    }

    notifyClose(id: string): void {
    }

    notifyOpen(position: Position): void {
    }

}

export class BacktestPositionManager implements PositionManager {
    private strategy: PositionStrategy = new DefaultPositionStrategy();
    private positions: Array<Position> = [];
    private openEvent$ = new Subject<Position>();
    private closeEvent$ = new Subject<Position>();

    private takeProfit: number;
    private stopLoss: number;

    constructor(
        private queryService: InstrumentQueryService,
        private plTracker: ProfitLossTracker,
    ) {
        this.plTracker.onExit(this.onExitTrigger)
    }

    finish() {
        this.closeEvent$.complete();
    }

    onClose$() {
        return this.closeEvent$.asObservable();
    }

    onOpen$() {
        return this.openEvent$.asObservable();
    }

    open(instrument: string, type: PositionType): void {
        if (!this.strategy.couldOpen(instrument)) return;

        this.createPosition(instrument, type);
    }

    setStrategy(strategy: PositionStrategy) {
        this.strategy = strategy;
    }

    setStopLoss(value: number) {
        this.stopLoss = value;
    }

    setTakeProfit(value: number) {
        this.takeProfit = value;
    }

    private onExitTrigger = (positionId: string, price: number, timestamp: number) => {
        const position = this.positions.find(({id}) => id === positionId);

        position.close = {
            timestamp,
            price
        };
        position.opened = false;

        this.closeEvent$.next(position);
    }

    private createPosition(instrument: string, type: PositionType) {
        const size = this.strategy.getPositionSize(instrument);

        const candle = this.queryService.getLast(instrument);

        const price = type === PositionType.LONG ? candle.ask.close : candle.bid.close;

        const position: Position = {
            instrument,
            type,
            open: {
                timestamp: candle.closedTime.unix(),
                price
            },
            close: null,
            size,
            opened: true,
            id: (this.positions.length + 1).toString()
        };

        this.positions.push(position);

        this.strategy.notifyOpen(position);

        this.plTracker.addTracking(position, this.takeProfit, this.stopLoss);

        this.openEvent$.next(position);
    }
}
