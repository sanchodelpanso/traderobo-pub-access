import { ProfitLossTracker } from '../backtest-position-manager';
import { InstrumentQueryService } from '@robo-trader/broker-api/models/instrument-query.service';
import { Position } from '../../../models/position-manager';
import { PositionType } from '../../../strategies/models/strategy';
import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';

export class BacktestPLTracker implements ProfitLossTracker {
    private onExitSubscriber: (id: string, price: number, timestamp: number) => void;

    constructor(private queryService: InstrumentQueryService) {}

    addTracking(position: Position, takeProfit: number, stopLoss: number) {
        const subscription = this.queryService
            .getCompleted$(position.instrument)
            .subscribe(candle => {
                const exit = this.isPLExit(position, takeProfit, stopLoss, candle);

                if (exit !== false) {
                    subscription.unsubscribe();

                    const closePrice = position.open.price + (position.type === PositionType.LONG ? exit : -exit);
                    this.onExitSubscriber(position.id, closePrice, candle.closedTime.unix())
                }
            });
    }

    onExit(callback: (id: string, price: number, timestamp: number) => void) {
        this.onExitSubscriber = callback;
    }

    private isPLExit(
        position: Position,
        takeProfit: number,
        stopLoss: number,
        candle: CandleData
    ): false | number {
        const currentPrice = (candle.bid.close + candle.ask.close) / 2;
        const spread = currentPrice * 0.00025;

        if (position.type === PositionType.LONG) {
            if (currentPrice >= position.open.price + takeProfit + spread) return takeProfit;
            if (currentPrice <= position.open.price - stopLoss - spread) return currentPrice - position.open.price;
        } else {
            if (currentPrice <= position.open.price - takeProfit - spread) return takeProfit;
            if (currentPrice >= position.open.price + stopLoss + spread) return position.open.price - currentPrice;
        }

        return false;
    }
}
