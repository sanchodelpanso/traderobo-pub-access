import { InstrumentQueryService } from '@robo-trader/broker-api/models/instrument-query.service';
import { Observable, Subject } from 'rxjs';
import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';
import { filter, finalize, map } from 'rxjs/operators';

export class BacktestQueryServiceHelper implements InstrumentQueryService {
    private historic$ = new Subject<[string, CandleData]>();
    private current$ = new Subject<[string, CandleData]>();
    private latest = new Map<string, CandleData>();

    private instruments$ = new Map<string, Observable<CandleData>>();
    private completedInstrumentsStreams: string[] = [];

    addInstrumentData$(instrument: string, candles$: Observable<CandleData>) {
        this.instruments$.set(instrument, candles$);

        candles$
            .pipe(finalize(() => {
                this.completedInstrumentsStreams.push(instrument);

                if (this.completedInstrumentsStreams.length === this.instruments$.size) {
                    this.historic$.complete();
                }
            }))
            .subscribe(candle => {
                if (!this.latest.get(instrument)) this.latest.set(instrument, null);

                this.latest.set(instrument, candle);
                this.historic$.next([instrument, candle]);
            });
    }

    getActiveStream$(instrument: string): Observable<CandleData> {
        return this.current$
            .asObservable()
            .pipe(
                filter(([code]) => code === instrument),
                map(([, candle]) => candle)
            );
    }

    getCompleted$(instrument: string): Observable<CandleData> {
        return this.historic$
            .asObservable()
            .pipe(
                filter(([code]) => code === instrument),
                map(([, candle]) => candle)
            );
    }

    getLast(instrument: string): CandleData {
        return this.latest.get(instrument);
    }
}
