import { max, min } from 'lodash';
import { pipe } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { CandleData, PriceData } from '@robo-trader/broker-api/models/candle-data.model';
import { PERIOD_TYPE_TIME_VALUE_MAP, PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { ObservablePipeline } from '../obseravable-pipeline';

export class SamplesAggregator implements ObservablePipeline<CandleData> {
    private readonly inputPeriodSeconds: number;
    private readonly outputPeriodSeconds: number;
    private readonly samplesRate: number;
    private accumulatedCandle: CandleData = null;

    constructor(inPeriodType: PeriodType, outPeriodType: PeriodType) {
        this.inputPeriodSeconds = PERIOD_TYPE_TIME_VALUE_MAP[inPeriodType].asSeconds();
        this.outputPeriodSeconds = PERIOD_TYPE_TIME_VALUE_MAP[outPeriodType].asSeconds();

        this.samplesRate = this.outputPeriodSeconds / this.inputPeriodSeconds;

        if (Math.round(this.samplesRate) !== this.samplesRate)
            throw new Error('Input period should be divisor for output period');
    }

    public pipe() {
        return pipe(
            filter(this.shouldPass),
            map(this.aggregate)
        )
    }

    private shouldPass = (candle: CandleData): boolean => {
        return this.samplesRate === 1
            || candle.time.unix() % this.outputPeriodSeconds === 0
            || this.accumulatedCandle !== null;
    }

    private aggregate = (candle: CandleData): CandleData => {
        if (this.samplesRate === 1) return candle;

        if (candle.time.unix() % this.outputPeriodSeconds === 0) {
            this.accumulatedCandle = candle;

            return this.accumulatedCandle;
        }

        this.accumulatedCandle = {
            ...this.accumulatedCandle,
            ask: this.mergePriceData(this.accumulatedCandle.ask, candle.ask),
            bid: this.mergePriceData(this.accumulatedCandle.bid, candle.bid),
            closedTime: candle.closedTime
        };

        return this.accumulatedCandle;
    }

    private mergePriceData(...prices: PriceData[]): PriceData {
        return prices
            .filter(price => price)
            .reduce<PriceData>((output, price) => {
                output.open = output.open || price.open;
                output.close = price.close;
                output.low = min([output.low, price.low]);
                output.high = max([output.high, price.high]);

                return output;
            }, {} as PriceData);
    }
}