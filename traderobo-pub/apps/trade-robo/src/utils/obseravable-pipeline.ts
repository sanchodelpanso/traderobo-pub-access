import { Observable, UnaryFunction } from 'rxjs';

export interface ObservablePipeline<Input, Output = Input> {
    pipe(): UnaryFunction<Observable<Input>, Observable<Output>>;
}
