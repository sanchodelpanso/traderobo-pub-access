import { Controller, Get, HttpException, HttpStatus, Param, Render } from '@nestjs/common';

import { BacktestService } from './services/backtest/backtest.service';
import moment = require('moment');

@Controller()
export class AppController {
  constructor(private backtestService: BacktestService) {
  }

  @Get('/')
  root() {
    return {OK: 'ok'}
  }

  @Get('/backtest/:symbol')
  async backtest(@Param('symbol') symbol: string) {
    const stopLoss = 0.0012;
    const takeProfit = stopLoss * 2;

    try {
      return this.backtestService
          .run(symbol, takeProfit, stopLoss)
          .then(positions => {
              const positionsFormated = positions
                  .slice(0, -1)
                  .map(position => ({
                      ...position,
                      openTime: moment.unix(position.open.timestamp).utc().format(),
                      closeTime: moment.unix(position.close.timestamp).utc().format(),
                      total: (position.close.price - position.open.price) * position.type
                  }));
            return {
                total: positionsFormated.reduce((total,pos) => total + pos.total, 0),
                ratio: positionsFormated.filter(p => p.total > 0).length / positionsFormated.length,
                positions: positionsFormated
            }
          });
    } catch (e) {
      console.error(e);
      throw new HttpException(e.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
