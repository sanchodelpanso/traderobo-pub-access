import 'reflect-metadata';

import { Strategy } from '../models/strategy';

export type NextFn<I = unknown> = (input: I, timestamp: number, strategy: Strategy<unknown>) => void;

type MiddlewareFn<I = unknown, O = I> = (next: NextFn<O>) => NextFn<I>;

export class IndicatorMeta<I> {
    private middlewares: MiddlewareFn[] = [];

    constructor(public propertyName: string) {}

    addMiddleware(middleware: MiddlewareFn) {
        this.middlewares.push(middleware);
    }

    runMiddlewares = (lastNext: NextFn) => (input: I, timestamp: number, strategy: Strategy<unknown>) => {
        this.middlewares.reduceRight(
            (next, middleware) => middleware(next),
            lastNext
        )(input, timestamp, strategy);
    }
}

export function StrategyIndicator<I, O>(transformer?: MiddlewareFn<I, O>): PropertyDecorator {
    return (target: Strategy<I>, propertyName: string) => {
        if (!Reflect.hasMetadata(Strategy.INDICATORS_META_KEY, target)) {
            Reflect.defineMetadata(Strategy.INDICATORS_META_KEY, [], target);
        }

        const meta = new IndicatorMeta(propertyName);

        if (transformer) {
            meta.addMiddleware(transformer);
        }

        Reflect
            .getMetadata(Strategy.INDICATORS_META_KEY, target)
            .push(meta);
    }
}
