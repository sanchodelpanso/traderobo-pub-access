import 'reflect-metadata';

import { Strategy } from '../models/strategy';
import { PERIOD_TYPE_TIME_VALUE_MAP } from '@robo-trader/broker-api/models/period-type.model';
import { IndicatorMeta } from './strategy-indicator';

export function InputSampler(samplesRate: number);
export function InputSampler<I>(samplesRate: number): PropertyDecorator {
    return (target: Strategy<I>, propertyKey) => {
        const indicatorsMeta: IndicatorMeta<I>[] = Reflect.getMetadata(Strategy.INDICATORS_META_KEY, target) || [];
        const meta = indicatorsMeta.find(indicator => indicator.propertyName === propertyKey);

        if (!meta) {
            throw new Error('InputSampler decorator could be only applied to property decorated with StrategyIndicator decorator');
        }

        meta.addMiddleware(next => (input: unknown, timestamp, strategy: Strategy<I>) => {
            const periodInSeconds = PERIOD_TYPE_TIME_VALUE_MAP[strategy.options.period].asSeconds() * samplesRate;
            if (timestamp % periodInSeconds === 0) {
                next(input, timestamp, strategy);
            }
        });
    }
}
