import * as moment from 'moment';
import { last } from 'lodash';

import { MACDIndicator, MACDOutput, MACDSign } from '../indicators/macd';
import { PositionType, Strategy } from './models/strategy';
import { EMAIndicator } from '../indicators/ema';
import { OscillatorPeakFinder } from '../indicators/helpers/oscillator-peak-finder';
import { PriceData } from '@robo-trader/broker-api/models/candle-data.model';
import { PERIOD_TYPE_TIME_VALUE_MAP, PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { NextFn, StrategyIndicator } from './decorators/strategy-indicator';
import { InputSampler } from './decorators/input-sampler';

type MACDZeroCrossOptions = {
    period: PeriodType;
    emaRelativeDeltaThreshold?: number;
    macdPeakMinThreshold?: number;
}

const DEFAULT_EMA_DELTA_THRESHOLD = 0.0005; // TODO: More declarative way like, WindowChange(60, '0.05%')
const DEFAULT_MACD_MIN_PEAK_THRESHOLD = 0.0001;

const Pick = (prop: keyof PriceData) =>
    (next: NextFn<number>) =>
        (candle: PriceData, timestamp: number, strategy: Strategy<PriceData>) =>
            next(candle[prop] as number, timestamp, strategy);

function calculateCoverage(period: number, value: number) {
    if (value === null) return 0;

    return value % period === 0 ? 1 : (value % period) / period;
}

function InputState(slowRate = 1): PropertyDecorator {
    return (target: Strategy<unknown>, propertyKey) => {
        Reflect.defineProperty(target, propertyKey, {
            get() {
                const self = this as Strategy<unknown>;
                const periodSeconds = PERIOD_TYPE_TIME_VALUE_MAP[self.options.period].asSeconds();

                return calculateCoverage(periodSeconds * slowRate, this.lastTimestamp);
            }
        });
    }
}

class SkipInputUtil {
    private skipNext = true;

    constructor(private periodsInSeconds: number[]) {}

    skip(closeTimestamp: number): boolean {
        if (!this.skipNext) return false;

        const allPeriodsClosed = this.periodsInSeconds
            .every(periodSeconds => closeTimestamp % periodSeconds === 0);
        this.skipNext = !allPeriodsClosed;

        return true;
    }
}

export class MACDZeroCrossStrategy extends Strategy<PriceData> {
    private static SLOW_RATE = 3;

    @StrategyIndicator<PriceData, number>(Pick('close'))
    private fastMACD = new MACDIndicator(12, 26, 9);

    @StrategyIndicator<PriceData, number>(Pick('close'))
    private fastEMA = new EMAIndicator(200);

    @InputSampler(MACDZeroCrossStrategy.SLOW_RATE)
    @StrategyIndicator<PriceData, number>(Pick('close'))
    private slowMACD = new MACDIndicator(12, 26, 9);

    @InputSampler(MACDZeroCrossStrategy.SLOW_RATE)
    @StrategyIndicator<PriceData, number>(Pick('close'))
    private slowEMA = new EMAIndicator(200);

    public options: MACDZeroCrossOptions;

    @InputState()
    private lastClosingState: number;
    @InputState(MACDZeroCrossStrategy.SLOW_RATE)
    private lastSlowClosingState: number;

    private slowMACDPeakFinder = new OscillatorPeakFinder(index => this.slowMACD.getLast(index).histogram);
    private skipUtil: SkipInputUtil;

    private get getLastTime(): string {
        return moment.unix(this.lastTimestamp).utc().format();
    }

    constructor(options: MACDZeroCrossOptions) {
        super(options);

        this.options = {
            emaRelativeDeltaThreshold: DEFAULT_EMA_DELTA_THRESHOLD,
            macdPeakMinThreshold: DEFAULT_MACD_MIN_PEAK_THRESHOLD,
            ...this.options
        };

        const mainPeriodInSecond = PERIOD_TYPE_TIME_VALUE_MAP[this.options.period].asSeconds();
        const slowPeriodInSeconds = mainPeriodInSecond * MACDZeroCrossStrategy.SLOW_RATE;
        this.skipUtil = new SkipInputUtil([mainPeriodInSecond, slowPeriodInSeconds]);
    }

    isPositionEntry() {
        if (!this.indicators.every(indicator => indicator.isReady(400))) return false; // TODO: Proper isReady check
        return this.isFastZeroCrossEntry();
    }

    isPositionExit(): boolean {
        return false;
    }

    append(input: PriceData, closeTimestamp: number) {
        if (!this.lastInput && this.skipUtil.skip(closeTimestamp))
            return;

        super.append(input, closeTimestamp);
    }

    private hadSlowMACDCrossedSignal(crossSign: MACDSign): boolean {
        return Math.sign(this.slowMACD.getLast().histogram) === crossSign;
    }

    private hadSlowMACDPassedPeakThreshold(crossSign: MACDSign): boolean {
        const lastPeak = this.slowMACDPeakFinder.previous();

        return Math.sign(lastPeak) !== crossSign
            && Math.abs(lastPeak) > this.lastInput.close * this.options.macdPeakMinThreshold;
    }

    private isMACDMatchesDirection(crossSign: MACDSign): boolean {
        return Math.sign(this.fastMACD.getLast().signal) !== crossSign
            // && Math.sign(this.slowMACD.getLast().macd) !== crossSign
            // && Math.sign(this.slowMACD.getLast().signal) !== crossSign;
    }

    private isFastEMAFollowDirection(crossSign: MACDSign): boolean {
        const relativeChange = this.fastEMA.getRelativeChange(60);

        if (Math.sign(relativeChange) !== crossSign) return false;

        return Math.abs(relativeChange) >= this.options.emaRelativeDeltaThreshold;
    }

    private isFastMACDHistogramFollowDirection(crossSign: MACDSign, deltaWindow = 3): boolean {
        return this.fastMACD.output
            .slice(-deltaWindow)
            .map(o => o.histogram)
            .reduce<Array<number[]>>((pairs, histogram) => {
                const lastPair = last(pairs);
                if (!lastPair) {
                    pairs.push([histogram]);

                    return pairs;
                }

                if (lastPair.length === 1) {
                    lastPair.push(histogram);
                } else {
                    pairs.push([lastPair[1], histogram])
                }

                return pairs;
            }, [])
            .every(([prev, next]) => Math.sign(next - prev) === crossSign);
    }

    private isFastZeroCrossEntry(): false | PositionType {
        if (this.lastClosingState < 0.8) return false; // TODO: replace number with constant

        const time = moment.unix(this.lastTimestamp).utc().format();
        // if (time === "2021-02-26T03:50:00Z") debugger;

        const crossedZero = this.fastMACD.isMainCrossedZero();
        if (!crossedZero) return false;

        if (!this.isFastMACDHistogramFollowDirection(crossedZero, 4)) return false;

        if (!this.isFastEMAFollowDirection(crossedZero)) return false;

        if (!this.isMACDMatchesDirection(crossedZero)) return false;

        // if (!this.hadSlowMACDCrossedSignal(crossedZero)) return false; // EXPERIMENT WITH SLOW CROSS (introduce waiting in queue)

        if (!this.hadSlowMACDPassedPeakThreshold(crossedZero)) return false;

        return crossedZero === MACDSign.POS ? PositionType.LONG : PositionType.SHORT;
    }
}
