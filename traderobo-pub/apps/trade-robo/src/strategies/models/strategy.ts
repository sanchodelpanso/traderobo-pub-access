import 'reflect-metadata';

import { PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { Indicator, IndicatorDebugState } from '../../indicators/models/indicator.model';
import { IndicatorMeta } from '../decorators/strategy-indicator';

// TODO: Extract to ../models folder
export enum PositionType {
    LONG = 1,
    SHORT = -1
}

export type StrategyDataSnapshot = IndicatorDebugState<unknown>[];


function BindMetadata(metaKey: string, defaultValue: any): PropertyDecorator {
    return function (
        target,
        propertyKey
    ) {
        if (defaultValue && !Reflect.hasMetadata(metaKey, target))
            Reflect.defineMetadata(metaKey, defaultValue, target);

        Reflect.defineProperty(target, propertyKey, {
            value: Reflect.getMetadata(metaKey, target)
        });
    };
}

export interface AbstractStrategy<Input> {
    lastInput: Input;
    lastTimestamp: number;

    isPositionEntry(): false | PositionType;
    isPositionExit(): boolean;
    append(input: Input, timestamp: number);
    update(input: Input, timestamp: number);
    snapshot(): StrategyDataSnapshot;
}

export interface StrategyOptions {
    period: PeriodType;
}

// TODO: Refactor to interface, all strategies should inherit interface, helper function could be added as composition
export abstract class Strategy<Input> implements AbstractStrategy<Input> {
    static INDICATORS_META_KEY = 'strategy:_indicators';

    @BindMetadata(Strategy.INDICATORS_META_KEY, [])
    private _indicators: IndicatorMeta<Input>[];
    protected get indicators(): Indicator<unknown, unknown>[] {
        return this._indicators.map(meta => Reflect.get(this, meta.propertyName));
    }

    private _lastInput: Input;
    private _lastTimestamp: number;

    public get lastInput(): Input {
        return this._lastInput;
    }

    public get lastTimestamp(): number {
        return this._lastTimestamp;
    }

    public abstract isPositionEntry(): false | PositionType;

    public abstract isPositionExit(): boolean;

    protected constructor(public options: StrategyOptions) {}

    public append(input: Input, closeTimestamp: number) {
        this._lastInput = input;
        this._lastTimestamp = closeTimestamp;

        this._indicators
            .forEach((meta) => {
                const indicatorInstance = Reflect.get(this, meta.propertyName) as Indicator<unknown, unknown>;

                meta.runMiddlewares(indicatorInstance.addSample.bind(indicatorInstance))(input, closeTimestamp, this);
            });
    }

    public update(input: Input, closeTimestamp: number) {
        if (!this._lastInput) {
            return;
        }

        this._lastInput = input;
        this._lastTimestamp = closeTimestamp;

        this._indicators
            .forEach((meta) => {
                const indicatorInstance = Reflect.get(this, meta.propertyName) as Indicator<unknown, unknown>;

                meta.runMiddlewares(indicatorInstance.updateLastSample.bind(indicatorInstance))(input, closeTimestamp, this);
            });
    }

    public snapshot(): StrategyDataSnapshot {
        return this.indicators.map((indicator: Indicator<unknown, unknown>) => indicator.debug());
    }
}
