type OscillatorValueSelector = (index: number) => number;

export class OscillatorPeakFinder {
    constructor(private selector: OscillatorValueSelector) {}

    private seekToReverse(index: number) : number {
        let value, currentSign = Math.sign(this.selector(index));

        do {
            value = this.selector(index++);
            currentSign = value === 0 ? 0 : currentSign;
        } while (Math.sign(value) === currentSign || currentSign === 0);

        return index;
    }

    private findPeak(startIndex: number): number {
        let peak = Math.abs(this.selector(startIndex));
        let sign = Math.sign(this.selector(startIndex));

        for (let i = startIndex; i >= 1; i--) {
            peak = Math.max(peak, Math.abs(this.selector(i)));
            sign = Math.sign(this.selector(i));
        }

        return sign * peak;
    }

    current(): number {
        return this.findPeak(1);
    }

    previous(): number {
        const lastReverseIndex = this.seekToReverse(1);

        return this.findPeak(lastReverseIndex);
    }
}
