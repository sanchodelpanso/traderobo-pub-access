export const toDegrees = (radians: number) => 180 * radians / Math.PI;
