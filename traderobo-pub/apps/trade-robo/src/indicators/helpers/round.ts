export function round(value: number, precision = 7) {
    return Number.parseFloat(value.toFixed(precision));
}