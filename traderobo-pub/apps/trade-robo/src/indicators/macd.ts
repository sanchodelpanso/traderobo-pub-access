import { Indicator, IndicatorDebugState } from './models/indicator.model';
import { SMAIndicator } from './sma';
import { EMAIndicator } from './ema';

export interface MACDOutput {
	macd: 		number;
	signal: 	number;
	histogram:	number;
	timestamp?:  any;
}

export enum MACDSign {
	POS = 1,
	NEG = -1
}

export class MACDIndicator extends Indicator<number, MACDOutput> {
	protected fastEMA: EMAIndicator;
	protected slowEMA: EMAIndicator;
	protected macdEMA: EMAIndicator;

	constructor(
		protected fastSamples: 		number,
		protected slowSamples: 		number,
		protected signalSamples: 	number,
		protected windowSize?:		number
	) {
		super();
		this.fastEMA = new EMAIndicator(fastSamples, fastSamples);
		this.slowEMA = new EMAIndicator(slowSamples, slowSamples);
		this.macdEMA = new EMAIndicator(signalSamples, this.windowSize);

		if (this.windowSize < 2) throw new Error('Requested windowSize less than minimal required');
	}

	addSample(sample: number, timestamp: number) {
		this.fastEMA.addSample(sample, timestamp);
		this.slowEMA.addSample(sample, timestamp);

		if (!this.slowEMA.isReady(this.slowSamples)) return;

		this.output.push(this.calculate(timestamp));

		if (this.windowSize && this.output.length > this.windowSize) {
			this.output.shift();
		}
	}

	updateLastSample(sample: number, timestamp: number) {
		this.fastEMA.updateLastSample(sample, timestamp);
		this.slowEMA.updateLastSample(sample, timestamp);

		if (!this.slowEMA.isReady(this.slowSamples)) return;

		this.output[this.output.length - 1] = this.calculate(timestamp);
	}

	isCrossedSignal(): false | MACDSign {
		return this.getLast(2).histogram * this.getLast(1).histogram < 0
			&& (this.getLast(1).histogram > 0 ? MACDSign.POS : MACDSign.NEG);
	}

	isMainCrossedZero(): false | MACDSign {
		const current = Math.sign(this.getLast(1).macd);
		const previous = Math.sign(this.getLast(2).macd);

		if (current === previous) return false;

		return Math.sign(current - previous);
	}

	protected calculate(timestamp: number): MACDOutput {
		if (!this.slowEMA.getLast()) {
			return {
				macd: null,
				signal: null,
				histogram: null,
			}
		}

		const macd = this.fastEMA.getLast() - this.slowEMA.getLast();
		this.macdEMA.addSample(macd, timestamp);

		return {
			macd: macd,
			signal: this.macdEMA.getLast(),
			histogram: macd - this.macdEMA.getLast()
		}
	}
}
