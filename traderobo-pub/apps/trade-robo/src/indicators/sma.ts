import { Indicator } from './models/indicator.model';
import { toDegrees } from './helpers/toDegrees';
import {round} from "./helpers/round";

export class SMAIndicator extends Indicator<number, number> {
	protected samples: Array<number> = []; // TODO: Refactor to Queue data structure

	constructor(
		protected samplesSize: number
	) {
		super();
	}

	isReady(): boolean {
		return this.getLast() !== null;
	}

	addSample(sample: number, timestamp: number): void {
		const result = this.updateSamples(sample) ? this.calculate() : null;

		this.output.push(result);
	}

	updateLastSample(sample: number, timestamp: number): void {
		const result = this.updateSamples(sample, true) ? this.calculate() : null;

		this.output.pop();
		this.output.push(result);
	}

	getLastAngle(): number {
		return toDegrees(
			Math.atan(
				this.getLast() - this.getLast(20)
			)
		)
	}

	protected updateSamples(sample: number, update = false): boolean {
		if (this.samples.length === this.samplesSize && !update) {
			this.samples.shift();
		}

		if (update) this.samples.pop();

		this.samples.push(sample);

		return this.samples.length === this.samplesSize;
	}

	protected calculate(): number {
		return round(
			this.samples.reduce<number>((sum, sample) => sum + sample, 0) / this.samplesSize
		);
	}
}
