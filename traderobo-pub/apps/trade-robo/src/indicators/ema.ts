import { Indicator, IndicatorDebugState } from './models/indicator.model';
import { SMAIndicator } from './sma';
import { toDegrees } from './helpers/toDegrees';
import {round} from "./helpers/round";

const DEFAULT_DELTA_WINDOW_SIZE = 20;

export class EMAIndicator extends Indicator<number, number> {
	private latestSample: number;
	private readonly weightMultiplier: number;

	constructor(
		protected samplerSize: 		number,
		protected windowSize?:		number
	) {
		super();
		this.weightMultiplier = 2 / (samplerSize + 1);
	}

	debug(): IndicatorDebugState<number> {
		return {
			...super.debug(2),
			meta: {
				relativeChange: this.getRelativeChange()
			}
		};
	}

	getRelativeChange(deltaWindow: number = DEFAULT_DELTA_WINDOW_SIZE): number {
		const previous = this.getLast(deltaWindow);
		const current = this.getLast(1);

		return (current - previous) / previous;
	}

	addSample(sample: number, timestamp: number): void {
		this.latestSample = sample;

		this.output.push(this.calculate());

		if (this.windowSize && this.output.length > this.windowSize) {
			this.output.shift();
		}
	}

	updateLastSample(sample: number, timestamp: number): void {
		this.latestSample = sample;

		this.output.pop();
		this.output.push(this.calculate());
	}

	protected calculate(): number {
		const result = this.getLast() === null
			? this.latestSample
			: this.getLast() + this.weightMultiplier * (this.latestSample - this.getLast());

		return round(result);
	}
}
