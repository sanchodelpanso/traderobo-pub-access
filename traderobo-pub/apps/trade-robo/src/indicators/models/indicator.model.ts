import {PeriodType} from "@robo-trader/broker-api/models/period-type.model";

export type IndicatorDebugState<O> = {
	output: O[],
	meta?: object
}

export abstract class Indicator<I, O> {
	output: Array<O> = [];
	protected lastCompleted: number;

	isReady(lastSamples?: number): boolean {
		return this.output.length >= lastSamples;
	}

	getLast(index = 1): O {
		return this.output.length >= index ? this.output[this.output.length - index] : null;
	}

	getLastProgress(): number {
		return this.lastCompleted;
	}

	debug(amount = 1): IndicatorDebugState<O> {
		return {
			output: this.output.slice(-amount)
		};
	}


	abstract addSample(sample: I, timestamp: number) : void;
	abstract updateLastSample(sample: I, timestamp: number) : void;
}
