import {HttpModule, Logger, Module} from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { AppController } from './app.controller';

import {BrokerApiModule} from "@robo-trader/broker-api";
import { TradingTaskService } from './services/trading-task/trading-task.service';
import {DataStorageManagerModule} from "@robo-trader/data-storage-manager";
import { BacktestService } from './services/backtest/backtest.service';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    HttpModule,
    BrokerApiModule,
    DataStorageManagerModule
  ],
  controllers: [AppController],
  providers: [
    TradingTaskService,
    BacktestService,
    Logger

    // StockScreenerService,
    // Trading212API,
    // {
    //   provide: StockQuotesAPIService,
    //   useClass: Trading212StockApiService,
    // }
  ]
})
export class AppModule {}
