import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InstrumentsCollectionService } from '@robo-trader/data-storage-manager';
import { Trading212QueryService } from '@robo-trader/broker-api/api/trading212/trading212-query.service';
import { PeriodType } from '@robo-trader/broker-api/models/period-type.model';

@Injectable()
export class WorkerService {
	logger = new Logger('WorkerService');
	constructor(
		private collectionService: InstrumentsCollectionService,
		private queryService: Trading212QueryService
	) {
		this.runJobs();
	}

	@Cron('0 0 */8 * * 1-5,7', {timeZone: 'America/New_York'}) // Every 8 hours Sunday-Friday
	async runJobs() {
		const available = await (await this.collectionService.getForex());

		const data = await this.queryService.getInstrumentsData(available, PeriodType.Minute).toPromise();

		data
			.forEach(instrument => this.collectionService.update(instrument.code, instrument.candles));

		this.logger.log('Last date fetched for:');
		this.logger.log(available.toString());
	}
}
