import { Logger, Module } from '@nestjs/common';
import {ScheduleModule} from "@nestjs/schedule";

import {DataStorageManagerModule} from "@robo-trader/data-storage-manager";
import { WorkerService } from './worker.service';
import {BrokerApiModule} from "@robo-trader/broker-api";

@Module({
  imports: [
      ScheduleModule.forRoot(),
      BrokerApiModule,
      DataStorageManagerModule
],
  providers: [Logger, WorkerService],
})
export class WorkerModule {}
