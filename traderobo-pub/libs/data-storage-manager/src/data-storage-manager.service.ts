import {createReadStream, mkdirSync, createWriteStream, readdirSync} from "fs";
import {join, dirname, basename} from "path";
import {createInterface} from "readline";
import {Injectable} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {EOL} from 'os';
import * as wildcard from 'wildcard'

import {STORAGE_DATA_FOLDER} from "../../../config.constants";

@Injectable()
export class DataStorageManagerService {
    private storageFolderPath: string;
    constructor(private configService: ConfigService) {
        this.storageFolderPath = this.configService.get(STORAGE_DATA_FOLDER);
    }

    getFileContent(file: string): Promise<Array<string>> {
        return new Promise((resolve => {
            const content = [];
            const path = join(this.storageFolderPath, file);

            const readlineInterface = createInterface({
                input: createReadStream(path, {flags: 'r+', encoding: 'utf8'})
            });

            readlineInterface.on('line', line => content.push(line));
            readlineInterface.on('close', () => resolve(content));
        }));
    }

    getFilesByPattern(path: string): Array<string> {
        const dir = dirname(path)
        const dirPath = join(this.storageFolderPath, dir);

        return readdirSync(dirPath)
            .filter(fileName => wildcard(path, join(dir, fileName)))
            .map(fileName => join(dir, fileName));
    }

    writeFileContents(file: string, data: string[], append = true) {
        const path = join(this.storageFolderPath, file);
        const ws = createWriteStream(path, {
            flags: append ? 'a' : 'w',
            encoding: 'utf8'
        });

        data.forEach(record => {
            ws.write(record);
            ws.write(EOL);
        });
        ws.end();
    }

    readFileLastLine(file: string): Promise<string> {
        // TODO: Provide way to seek to last line
 
        return new Promise(resolve => {
            const storageFolderPath = this.configService.get(STORAGE_DATA_FOLDER);
            const path = join(storageFolderPath, file);

            const readlineInterface = createInterface({
                input: createReadStream(path, {flags: 'a+'})
            });

            let lastLine;
            readlineInterface.on('line', currentLine => lastLine = currentLine);
            readlineInterface.on('close', () => resolve(lastLine));
        });
    }

    checkDir(folder: string) {
        const path = join(this.storageFolderPath, folder);

        mkdirSync(path, { recursive: true });
    }
}
