import { Test, TestingModule } from '@nestjs/testing';
import { DataStorageManagerService } from './data-storage-manager.service';

describe('DataStorageManagerService', () => {
  let service: DataStorageManagerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DataStorageManagerService],
    }).compile();

    service = module.get<DataStorageManagerService>(DataStorageManagerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
