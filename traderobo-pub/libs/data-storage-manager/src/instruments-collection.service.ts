import {Injectable} from '@nestjs/common';
import { get, set } from 'lodash'
import * as moment from 'moment'

import {
    AVAILABLE_FOREX
} from "@robo-trader/data-storage-manager/data-storage-manager.constans";
import {DataStorageManagerService} from "@robo-trader/data-storage-manager/data-storage-manager.service";
import {CandleData} from "@robo-trader/broker-api/models/candle-data.model";
import {from, Observable} from "rxjs";
import { fromPromise } from "rxjs/internal-compatibility";
import {concatAll, concatMap, map} from "rxjs/operators";
import { PERIOD_TYPE_TIME_VALUE_MAP, PeriodType } from '@robo-trader/broker-api/models/period-type.model';

class CandleDataCSVTransformer<T> {
    props = [
        'ask.open',
        'ask.high',
        'ask.low',
        'ask.close',
        'bid.open',
        'bid.high',
        'bid.low',
        'bid.close',
        'time',
        'volume'
    ];

    fromCSV = (line: string): T => {
        if (!line) return null;

        return line
            .split(',')
            .reduce((model, field, index) =>
                set(model, this.props[index], this.props[index] === 'time' ? moment(field) : Number.parseFloat(field)
            ), {}) as T;
    }

    toCSV = (model: T): string => {
        return this.props
            .map(field => get(model, field))
            .join(',')
    }
}

const RECORDS_FOLDER = 'records';
const DATE_FORMAT = 'MM-YYYY';
const DATE_FORMAT_REGEX = /\d{2}-\d{4}/;

function sortByDate(fileNames: string[]): string[] {
    return fileNames
        .map(file => [
            file,
            get(file.match(DATE_FORMAT_REGEX), '0', null)
        ])
        .map(([file, date]) => [file, moment(date, DATE_FORMAT)])
        .sort((one, second) => {
            return one[1].isBefore(second[1]) ? -1 : 1;
        })
        .map(([file]) => file);
}

const SERIALIZED_PERIOD = PeriodType.Minute;

@Injectable()
export class InstrumentsCollectionService {
    constructor(private dataStorageManager: DataStorageManagerService) {
        this.dataStorageManager.checkDir(RECORDS_FOLDER);
    }

    getForex(): Promise<Array<string>> {
        return this.dataStorageManager.getFileContent(`${AVAILABLE_FOREX}.csv`);
    }

    getInstrumentLastRecord(code: string, date: string): Promise<CandleData> {
        return this.dataStorageManager
            .readFileLastLine(`${RECORDS_FOLDER}/${code}-${date}.csv`)
            .then(new CandleDataCSVTransformer<CandleData>().fromCSV)
    }

    getInstrumentData$(code: string): Observable<CandleData> {
        const files = this.dataStorageManager
            .getFilesByPattern(`${RECORDS_FOLDER}/${code}-*.csv`);
        const filesByAscDate = sortByDate(files);

        return from(filesByAscDate)
            .pipe(
                concatMap(file => fromPromise(this.dataStorageManager.getFileContent(file))),
                concatAll(),
                map(new CandleDataCSVTransformer<Partial<CandleData>>().fromCSV),
                map(candle => ({
                    ...candle,
                    closedTime: moment(candle.time).add(PERIOD_TYPE_TIME_VALUE_MAP[SERIALIZED_PERIOD].asSeconds(), 'seconds')
                } as CandleData))
            )
    }

    async update(code: string, allCandles: Array<CandleData>): Promise<void> {
        const dataByMonths = allCandles.reduce<Map<string, CandleData[]>>((map, candle: CandleData) => {
            const date = candle.time.utc().format(DATE_FORMAT);
            if (!map.get(date)) map.set(date, [candle]);
            else map.get(date).push(candle);

            return map;
        }, new Map());

        dataByMonths.forEach((async (candles: CandleData[], date: string) => {
            const last = await this.getInstrumentLastRecord(code, date);
            console.log(last);

            const newCandles = !last
                ? candles
                : candles.filter(candle => candle.time.isAfter(last.time));

            const serialized = newCandles.map(candle => {
                return new CandleDataCSVTransformer().toCSV({
                    ...candle,
                    time: candle.time.toJSON() // TODO: extract to CSVTransformer class, libs/broker-api/src/api/alphavantage/utils/csv-candle-data-serializer.ts
                })
            });
    
            this.dataStorageManager.writeFileContents(`${RECORDS_FOLDER}/${code}-${date}.csv`, serialized);
        }));
    }
}
