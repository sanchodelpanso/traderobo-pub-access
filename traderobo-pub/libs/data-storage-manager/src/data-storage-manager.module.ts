import {Module} from '@nestjs/common';

import { DataStorageManagerService } from './data-storage-manager.service';
import { GlobalConfigModule } from "@robo-trader/global-config";
import {InstrumentsCollectionService} from "@robo-trader/data-storage-manager/instruments-collection.service";

@Module({
  imports: [GlobalConfigModule],
  providers: [DataStorageManagerService, InstrumentsCollectionService],
  exports: [DataStorageManagerService, InstrumentsCollectionService],
})
export class DataStorageManagerModule {
}
