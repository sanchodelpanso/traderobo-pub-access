export const DATA_STORAGE_MANAGER_MODULE_OPTIONS = 'DATA_STORAGE_MANAGER_MODULE_OPTIONS';

export const QUOTES_FOLDER = 'quotes';

export const AVAILABLE_US_STOCKS = 'available-us-stocks';
export const AVAILABLE_FOREX = 'available-forex';
