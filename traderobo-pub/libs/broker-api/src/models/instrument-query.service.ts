import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';
import { Observable } from 'rxjs';

export interface InstrumentQueryService {
    getLast(instrument: string): CandleData;

    getCompleted$(instrument: string): Observable<CandleData>;

    getActiveStream$(instrument: string): Observable<CandleData>;
}
