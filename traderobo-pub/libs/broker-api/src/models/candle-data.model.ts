import { Moment } from 'moment';

export interface PriceData {
    open: number;
    high: number;
    low: number;
    close: number;
}

export interface CandleData {
    ask: PriceData;
    bid: PriceData;
    time: Moment;
    closedTime: Moment;
    volume: number;
    openTimestamp?: number;
    closeTimestamp?: number;
}
