import * as moment  from 'moment';
import {Duration} from "moment";
import {Indicator} from "../../../../apps/trade-robo/src/indicators/models/indicator.model";

export enum PeriodType {
    Minute = 'ONE_MINUTE',
    FiveMinutes = 'FIVE_MINUTES',
    TenMinutes = 'TEN_MINUTES',
    FifteenMinutes = 'FIFTEEN_MINUTES',
    ThirtyMinutes = 'THIRTY_MINUTES',
    Hour = 'ONE_HOUR',
    Day = 'ONE_DAY'
}

export const PERIOD_TYPE_TIME_VALUE_MAP: {
    [key in PeriodType]: Duration;
} = {
    [PeriodType.Minute]:			moment.duration({minutes: 1}),
    [PeriodType.FiveMinutes]:		moment.duration({minutes: 5}),
    [PeriodType.TenMinutes]:		moment.duration({minutes: 10}),
    [PeriodType.FifteenMinutes]:	moment.duration({minutes: 15}),
    [PeriodType.ThirtyMinutes]:		moment.duration({minutes: 30}),
    [PeriodType.Hour]:				moment.duration({hours: 1}),
    [PeriodType.Day]:				moment.duration({hours: 24})
};
