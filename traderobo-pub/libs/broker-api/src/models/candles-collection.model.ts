import { PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';

export interface CandlesCollection {
	code: string;
	periodType: PeriodType;
	candles: Array<CandleData>;
}
