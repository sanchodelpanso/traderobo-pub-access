import { HttpModule, Logger, Module } from '@nestjs/common';

import { Trading212QueryService } from '@robo-trader/broker-api/api/trading212/trading212-query.service';
import { TimeSeriesStockApiService } from '@robo-trader/broker-api/api/alphavantage/time-series-stock-api.service';
import { InstrumentSubscriptionService } from '@robo-trader/broker-api/services/instrument-subscription.service';
import { ScheduleModule } from '@nestjs/schedule';
import { InstrumentQueryBacktestService } from '@robo-trader/broker-api/api/trading212/instrument-query-backtest.service';
import { InstrumentSubscriptionBacktestService } from '@robo-trader/broker-api/services/instrument-subscription-backtest.service';

@Module({
	imports: [HttpModule, ScheduleModule.forRoot()],
	providers: [
		TimeSeriesStockApiService,
		Trading212QueryService,
		InstrumentSubscriptionService,
		InstrumentQueryBacktestService,
		InstrumentSubscriptionBacktestService,
		Logger
	],
	exports: [
		TimeSeriesStockApiService,
		Trading212QueryService,
		InstrumentSubscriptionService,
		InstrumentQueryBacktestService,
		InstrumentSubscriptionBacktestService
	],
})
export class BrokerApiModule {
}
