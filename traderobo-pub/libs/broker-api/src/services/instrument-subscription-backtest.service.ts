import { Injectable } from '@nestjs/common';
import { Observable, Subject, of, forkJoin } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import {PERIOD_TYPE_TIME_VALUE_MAP, PeriodType} from '../models/period-type.model';
import { InstrumentData } from '@robo-trader/broker-api';
import { InstrumentQueryBacktestService } from '@robo-trader/broker-api/api/trading212/instrument-query-backtest.service';

const periodTypeToTime = (periodType: PeriodType) => PERIOD_TYPE_TIME_VALUE_MAP[periodType].asMinutes();

const timeToPeriodType = (time: number): PeriodType => Object
	.entries(PERIOD_TYPE_TIME_VALUE_MAP)
	.find(([periodType, duration]) => time === duration.asMinutes())?.shift() as PeriodType;

@Injectable()
export class InstrumentSubscriptionBacktestService {
	private everyMinute$ = new Subject<number>();
	private currentMinute = 0;

	constructor(private queryService: InstrumentQueryBacktestService) {}

	getLatestData$(instrument: string, periodTypes: PeriodType[]): Observable<InstrumentData[]> {
		const periodsInMinutes = periodTypes
			.map(periodTypeToTime);
		const minPeriod = periodsInMinutes.slice().sort((a, b) => a < b ? -1 : 1).shift();

		const stream$ = this.everyMinute$
			.asObservable()
			.pipe(
				mergeMap(currentTimeMinute => forkJoin(
					periodsInMinutes
						.filter(period => currentTimeMinute % period === 0)
						.map(timeToPeriodType)
						.map(periodType => forkJoin({
							periodType: of(periodType),
							candle: this.queryService.getLastClosedCandle(instrument, periodType)
						}))
					)
				),
				map(data => {
					if (data.every(item => item.candle)) {
						setImmediate(() => this.mainSyncTask(minPeriod))
						return data;
					} else {
						this.everyMinute$.complete();
						return [];
					}
				})
			);

		setImmediate(() => this.mainSyncTask(minPeriod));

		return stream$;
	}

	private mainSyncTask(step: number) {
		this.everyMinute$.next(this.currentMinute);

		this.currentMinute += step;
		if (this.currentMinute === 60) this.currentMinute = 0;
	}
}
