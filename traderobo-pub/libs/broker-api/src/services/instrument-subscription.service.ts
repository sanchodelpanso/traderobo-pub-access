import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Observable, Subject, from, of, forkJoin } from 'rxjs';
import { filter, mergeMap, map } from 'rxjs/operators';
import * as moment from 'moment';

import { PeriodType } from '../models/period-type.model';
import { CandleData } from '../models/candle-data.model';
import { Trading212QueryService } from '../api/trading212/trading212-query.service';

export type InstrumentData = {
	periodType: PeriodType,
	candle: CandleData
}

const PERIOD_TYPE_TIME_VALUE_MAP ={
	[PeriodType.Minute]:			1,
	[PeriodType.FiveMinutes]:		5,
	[PeriodType.FifteenMinutes]:	15,
	[PeriodType.ThirtyMinutes]:		30,
	[PeriodType.Hour]:				60,
	[PeriodType.Day]:				24 * 60
};

const periodTypeToTime = (periodType: PeriodType) => PERIOD_TYPE_TIME_VALUE_MAP[periodType];

const timeToPeriodType = (time: number): PeriodType => Object
	.entries(PERIOD_TYPE_TIME_VALUE_MAP)
	.find(([periodType, timeValue]) => time === timeValue)?.shift() as PeriodType;


@Injectable()
export class InstrumentSubscriptionService {
	private everyMinute$ = new Subject<number>();

	constructor(private queryService: Trading212QueryService) {}

	getLatestData$(instrument: string, periodTypes: PeriodType[]): Observable<InstrumentData[]> {
		const periods = periodTypes
			.map(periodTypeToTime);

		return this.everyMinute$
			.asObservable()
			.pipe(
				mergeMap(minute => forkJoin(
					periods
						.filter(period => minute % period === 0)
						.map(timeToPeriodType)
						.map(periodType => forkJoin({
							periodType: of(periodType),
							candle: this.queryService.getLastClosedCandle(instrument, periodType)
						}))
					)
				)
			)
	}

	@Cron('15 * * * * *')
	private mainSyncTask() {
		const currentMinute = moment().minute();

		this.everyMinute$.next(currentMinute);
	}
}
