import { Injectable } from '@nestjs/common';
import { from, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { PeriodType } from '../../models/period-type.model';
import { CandleData } from '../../models/candle-data.model';
import { Trading212QueryService } from './trading212-query.service';

@Injectable()
export class InstrumentQueryBacktestService {
	static MAX_DATA_SIZE = 500;

	private futureData: Map<PeriodType, CandleData[]> = new Map();

	constructor(private queryService: Trading212QueryService) {}

	getInstrumentData(ticker: string, periodType: PeriodType, period = 50, onlyClosed= true): Observable<CandleData[]> {
		return this.queryService
			.getInstrumentData(ticker, periodType, InstrumentQueryBacktestService.MAX_DATA_SIZE)
			.pipe(
				map(data => {
					const response = data.slice(0, period);

					this.futureData.set(periodType, data.slice(period));

					return response;
				})
			)
	}

	getLastClosedCandle(ticker: string, periodType: PeriodType): Observable<CandleData> {
		const closestInFuture = this.futureData.get(periodType).shift();

		return of(closestInFuture);
	}
}
