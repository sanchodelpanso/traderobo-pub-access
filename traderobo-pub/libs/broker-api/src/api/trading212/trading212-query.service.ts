import { HttpService, Injectable, Logger } from '@nestjs/common';
import { PERIOD_TYPE_TIME_VALUE_MAP, PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { CandlesCollection } from '@robo-trader/broker-api/models/candles-collection.model';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { head, last } from 'lodash';
import * as moment from 'moment';

import { CandleData } from '@robo-trader/broker-api/models/candle-data.model';
import { CandleDataResponse } from './models/candle-data-response.model';

@Injectable()
export class Trading212QueryService {
	static MAX_DATA_SIZE = 500;

	private readonly host = 'https://live.trading212.com';
	private readonly commonHeaders = {
		'accept': 'application/json',
		'cache-control': 'no-cache',
		'content-type': 'application/json',
		'pragma': 'no-cache'
	};

	constructor(private http: HttpService, private logger: Logger) {}

	private filterOnlyClosed = (onlyClosed: boolean) => (candle: CandleData) => !onlyClosed || !candle.closeTimestamp || !candle.openTimestamp;

	getInstrumentsData(tickers: Array<string>, period: PeriodType, size = Trading212QueryService.MAX_DATA_SIZE, onlyClosed = true): Observable<CandlesCollection[]> {
		const data = {
			candles: tickers.map(ticker => ({
				ticker,
				size,
				period,
				includeFake: false
			}))
		};

		return this.http
			.post('charting/v2/batch', data, {
				baseURL: this.host,
				headers: this.commonHeaders,
			}).pipe(
				map(response => response.data),
				map(data => data.candles),
				map(data => data.map(({request, result}) => ({
					periodType: period,
					code: request.ticker,
					candles: result
						.map(this.formatCandleTime(period))
						.filter(this.filterOnlyClosed(onlyClosed))
				})))
			);
	}

	getInstrumentData(ticker: string, periodType: PeriodType, size = 50, onlyClosed = true): Observable<CandleData[]> {
		return this
			.getInstrumentsData([ticker], periodType, (onlyClosed ? size + 1 : size), onlyClosed)
			.pipe(
				map(head),
				map(collection => collection.candles),
				map(candles => candles.filter(candle => !onlyClosed || !candle.openTimestamp || !candle.closeTimestamp))
			)
	}

	getLastClosedCandle(ticker: string, periodType: PeriodType): Observable<CandleData> {
		return this
			.getInstrumentData(ticker, periodType, 1)
			.pipe(map(last))
	}

	private formatCandleTime = (period: PeriodType) => (candle: CandleDataResponse): Partial<CandleData> => {
		return {
			...candle,
			time: moment(candle.timestamp),
			closedTime: moment(candle.timestamp).add(PERIOD_TYPE_TIME_VALUE_MAP[period].asSeconds(), 'seconds'),
		}
	}
}
