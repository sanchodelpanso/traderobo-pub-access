import { PriceData } from "@robo-trader/broker-api/models/candle-data.model";

export interface CandleDataResponse {
    ask: PriceData;
    bid: PriceData;
    volume: number;
    timestamp: number;
    openTimestamp?: number;
    closeTimestamp?: number;
    fake?: boolean;
}