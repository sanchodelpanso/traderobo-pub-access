import { CandleData, PriceData } from '@robo-trader/broker-api/models/candle-data.model';
import * as moment from 'moment-timezone';

type TimeSeriesCandle = {
	timestamp: number;
	open: number;
	close: number;
	high: number;
	low: number;
	volume: number;
}

type FieldSchema = {
	name: string;
	transform: (serializedValue: string) => any;
}

const DEFAULT_STOCK_SPREAD = 0.05;
const DEFAULT_FX_SPREAD = 1.0001;

/**
 * TODO: Create Decorator
 * @CSVSerializeSchema({
 *   time: {
 *       position: 0,
 *       transform: Number
 *   }
 * })
 */
export class TimeSeriesCandleSerializeSchema {
	private props: Array<FieldSchema> = [{
		name: 'timestamp',
		transform: data => moment.tz(data, 'America/New_York').valueOf()
	},
	{
		name: 'open',
		transform: Number
	},
	{
		name: 'high',
		transform: Number
	},
	{
		name: 'low',
		transform: Number
	},

	{
		name: 'close',
		transform: Number
	},
	{
		name: 'volume',
		transform: Number
	}];

	constructor(fields: Array<string>) {
		fields.every((fieldName, index) => {
			const position = this.getPositionByName(fieldName);

			if (position === index) return true;

			throw new Error(`Invalid schema. Field '${fieldName}' not found in configuration`);
		});
	}

	public getPositionByName(name: string): number {
		return this.props
			.findIndex((prop) =>  prop.name === name);
	}

	public getByPosition(position: number): FieldSchema  {
		return this.props
			.find((prop, index) =>  index === position);
	}
}

export class CSVCandleDataDeserializer {
	constructor(
		private schema: TimeSeriesCandleSerializeSchema,
		private spread: number = DEFAULT_FX_SPREAD
	) {}

	private getPriceData(raw: TimeSeriesCandle, applySpread = false): PriceData {
		return {
			open: applySpread ? raw.open * this.spread : raw.open,
			close: applySpread ? raw.close * this.spread : raw.close,
			high: applySpread ? raw.high * this.spread : raw.high,
			low: applySpread ? raw.low * this.spread : raw.low
		}
	}

	deserialize(lines: Array<string>): Array<CandleData> {
		return lines
			.map(line => {
				const raw = line
					.split(',')
					.reduce<TimeSeriesCandle>((model, value, index) => {
						const field = this.schema.getByPosition(index);
						model[field.name] = field.transform(value);

						return model
					}, {} as TimeSeriesCandle);

				return {
					time: moment(raw.timestamp),
					closedTime: moment(raw.timestamp),
					volume: raw.volume,
					ask: this.getPriceData(raw, true),
					bid: this.getPriceData(raw)
				}
			})
	}
}
