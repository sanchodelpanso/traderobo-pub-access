import {PeriodType} from "@robo-trader/broker-api/models/period-type.model";
import { TimeSeriesInterval } from '@robo-trader/broker-api/api/alphavantage/models/time-series-interval.model';

export const periodTypeToTimeSeriesInterval = (period: PeriodType): TimeSeriesInterval => ({
    [PeriodType.Minute]:            TimeSeriesInterval.Minute,
    [PeriodType.FiveMinutes]:       TimeSeriesInterval.FiveMinutes,
    [PeriodType.FifteenMinutes]:    TimeSeriesInterval.FifteenMinutes,
    [PeriodType.ThirtyMinutes]:     TimeSeriesInterval.ThirtyMinutes,
    [PeriodType.Hour]:              TimeSeriesInterval.Hour
})[period];
