export enum TimeSeriesInterval {
    Minute = '1min',
    FiveMinutes = '5min',
    FifteenMinutes = '15min',
    ThirtyMinutes = '30min',
    Hour = '60min'
}
