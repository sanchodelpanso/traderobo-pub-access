import { memoize } from 'lodash'
import { HttpService, Injectable, Logger } from '@nestjs/common';
import { PeriodType } from '@robo-trader/broker-api/models/period-type.model';
import { CandlesCollection } from '@robo-trader/broker-api/models/candles-collection.model';
import { periodTypeToTimeSeriesInterval } from '@robo-trader/broker-api/api/alphavantage/utils/periodTypeToTimeSeriesInterval';
import {
	CSVCandleDataDeserializer,
	TimeSeriesCandleSerializeSchema
} from '@robo-trader/broker-api/api/alphavantage/utils/csv-candle-data-serializer';

@Injectable()
export class TimeSeriesStockApiService {
	private readonly host = 'https://www.alphavantage.co';

	constructor(private http: HttpService, private logger: Logger) {
	}

	queryIntradayExtended(symbol: string, periodType: PeriodType, month): Promise<Array<string>> {
		return this.http
			.get('/query', {
				baseURL: this.host,
				params: {
					function: 'TIME_SERIES_INTRADAY_EXTENDED',
					symbol,
					interval: periodTypeToTimeSeriesInterval(periodType),
					slice: `year1month${month}`,
					adjusted: false,
					apikey: 'UNFDE02PZDQ3PRXC' // OVFGVIQGFPUSBT2E
				},
				responseType: 'text'
			})
			.toPromise()
			.then(response => response.data)
			.then(data => data.split('\n'))
	}

	queryIntraday(symbol: string, periodType: PeriodType): Promise<Array<string>> {
		return this.http
			.get('/query', {
				baseURL: this.host,
				params: {
					function: 'TIME_SERIES_INTRADAY',
					symbol,
					interval: periodTypeToTimeSeriesInterval(periodType),
					datatype: 'csv',
					outputsize: 'full',
					adjusted: false,
					apikey: 'UNFDE02PZDQ3PRXC' // OVFGVIQGFPUSBT2E
				},
				responseType: 'text'
			})
			.toPromise()
			.then(response => response.data)
			.then(data => data.split('\n'))
	}

	getIntardayData = async (symbol: string, periodType: PeriodType): Promise<CandlesCollection> => {
		const data = await this.queryIntraday(symbol, periodType);
		// TODO: Refactor to proper CSV Parser
		const schema = new TimeSeriesCandleSerializeSchema(data[0].trim().split(','));

		const serializer = new CSVCandleDataDeserializer(schema);

		return {
			code: symbol,
			periodType,
			candles: serializer.deserialize(data.slice(1))
		}
	}
}
