import { resolve } from "path"
import {STORAGE_DATA_FOLDER} from "./config.constants";

export default () => ({
    [STORAGE_DATA_FOLDER]: resolve(process.cwd(), './data')
});
